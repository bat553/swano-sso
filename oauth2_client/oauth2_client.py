#  swano-sso
#  oauth2_client.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25

import os
import uuid

import requests
from flask import Flask, redirect, url_for, session, request
from flask_oauthlib.client import OAuth, OAuthException

app = Flask(__name__)

cache = {}

oauth = OAuth()

swano_sso = oauth.remote_app(
    "swano_sso",
    access_token_url="http://127.0.0.1:8000/oauth2/access_token",
    authorize_url="http://127.0.0.1:8000/oauth2/authorize",
    consumer_key=os.getenv("CLIENT_ID"),
    consumer_secret=os.getenv("CLIENT_SECRET"),
    request_token_params={"scope": "admin"},
    base_url="http://127.0.0.1:8000/",
)

app.config.update(
    TESTING=True,
    SECRET_KEY="cj@Tm^kH6a6cNbCTj*fGW*GueGyupzABBhfYCx#9H@fd&",
)


def get_user(access_token):
    resp = swano_sso.get("api/me", token=access_token)
    if resp.status != 200:
        return None
    else:
        return resp.data


@app.route("/")
def index():
    if session.get("swano_token"):
        user = get_user(session.get("swano_token"))
        if user:
            index = f"Bonjour {user.get('prenom')} {user.get('nom')}<br>"
        else:
            index = "<h3 style='color: red'>Impossible de récupérer les info de l'utilisateur</h3>"
        index += "<a href='/logout'>Logout</a>"
    else:
        index = "<a href='/login_view'>Login</a>"
    return index


@app.route("/login_view")
def login():
    state = uuid.uuid4().hex
    session["state"] = state
    return swano_sso.authorize(callback="http://127.0.0.1:5000/callback", state=state, hide_grant=True)


@app.route("/callback")
def callback():
    if not request.args.get("state") or request.args.get("state") != session.get("state"):
        return "State invalide"
    next_url = request.args.get("next") or url_for("index")
    try:
        resp = swano_sso.authorized_response()
    except OAuthException as e:
        return e.data

    if resp is None:
        return redirect(next_url)

    session["swano_token"] = resp.get("access_token")

    return redirect(url_for("index"))


@app.route("/logout")
def logout():
    requests.post(
        swano_sso.base_url + "oauth2/revoke",
        data={"token": session.get("swano_token"), "token_type_hint": "access_token",
              "client_id": swano_sso.consumer_key, "client_secret": swano_sso.consumer_secret},
    )
    session["swano_token"] = None
    session["utilisateur"] = None
    return redirect(url_for("index"))


if __name__ == "__main__":
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get("PORT", 5000))
    app.run(host="127.0.0.1", port=port, debug=True)
