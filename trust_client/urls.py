from django.urls import path

from trust_client import views

urlpatterns = [
    path("", views.status, name="oauth2_status_view"),
    path("login", views.login_view, name="oauth2_login_view"),
    path("callback", views.authorize, name="callback_view"),
    path("logout", views.logout_view, name="logout_view"),
]
