import certifi
import requests
from django.apps import AppConfig


def get_certs():
    try:
        print("Checking connection to SSO...")
        requests.get("https://certs.swano-lab.net")
        print("Connection to SSO OK.")
    except requests.exceptions.SSLError:
        print("SSL Error. Adding custom certs to Certifi store...")

        caroot = requests.get("http://certs.swano-lab.net/certs/Swano_Primary_CA_v01.crt")
        caserver = requests.get("http://certs.swano-lab.net/certs/Swano_Servers_CA_v01.crt")

        cafile = certifi.where()
        CA_ENTRY_DESC = b"""
# Issuer: CN=Swano Primary CA v01 O=Swano Corporation
# Subject: CN=Swano Primary CA v01 O=Swano Corporation
# Label: "CA Root Swano Corporation"
    """

        with open(cafile, "ab") as outfile:
            outfile.write(CA_ENTRY_DESC)
            outfile.write(caroot.content)
            outfile.write(caserver.content)
        print("That might have worked.")


class TrustClientConfig(AppConfig):
    name = "trust_client"

    def ready(self):
        get_certs()
