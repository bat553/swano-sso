import os

import requests
from django.conf import settings
from django.contrib.auth import login, logout
from django.http import JsonResponse
from django.core.exceptions import ValidationError

# Create your views here.
from authlib.integrations.django_client import OAuth
from django.shortcuts import redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from app_sso.models import OAuth2Token

oauth = OAuth()

oauth.register(
    name="swano_sso",
    client_id=os.environ.get("TRUST_CLIENT_ID"),
    client_secret=os.environ.get("TRUST_CLIENT_SECRET"),
    access_token_url=f"{settings.INTERNAL_APP}/oauth2/access_token",
    access_token_params=None,
    authorize_url=f"{settings.BASE_URL}/oauth2/authorize",
    authorize_params=None,
    api_base_url=settings.BASE_URL,
    client_kwargs={"scope": "profile"},
)


def check_host(request):
    print(request.headers.get("HOST"))
    if request.headers and (
            request.headers.get("HOST") == "trust.swano-lab.net" or request.headers.get("HOST") == "localhost:8000"
    ):
        return True
    raise ValidationError("Host invalide", code="invalid_host")


@require_http_methods(["GET"])
def login_view(request):
    check_host(request)
    # build a full authorize callback uri
    swano_sso = oauth.create_client("swano_sso")
    redirect_uri = settings.TRUST_APP_URL + reverse('callback_view')
    return swano_sso.authorize_redirect(request, redirect_uri, hide_grant=True)


@require_http_methods(["GET"])
def authorize(request):
    check_host(request)
    swano_sso = oauth.create_client("swano_sso")
    token = swano_sso.authorize_access_token(request)
    try:
        token = OAuth2Token.objects.get(access_token=token.get("access_token"))
        utilisateur = token.user
    except token.DoesNotExist:
        raise ValidationError("Token inexistant")
    if utilisateur:
        login(request, utilisateur)
        request.session["access_token"] = token.access_token
        return redirect("/")
    raise ValidationError("Utilisateur introuvable", code="unknown_user")


@require_http_methods(["GET"])
def status(request):
    if request.user and request.user.is_authenticated:
        return JsonResponse(data=dict(statut="OK"))
    return JsonResponse(data=dict(statut="NOK"), status=403)


@require_http_methods(["POST"])
@csrf_exempt
def logout_view(request):
    swano_sso = oauth.create_client("swano_sso")
    if request.session.get("access_token"):
        requests.post(f'{settings.INTERNAL_APP}/oauth2/revoke',
                      data={"token": request.session.get("access_token"), "token_type_hint": "access_token"},
                      auth=(swano_sso.client_id, swano_sso.client_secret)
                      )
    logout(request)
    return JsonResponse(data=dict(detail="Vous êtes déconnecté."))
