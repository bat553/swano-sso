from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("APP_SECRET")

DEBUG = False

BASE_URL = "https://sso.swano-lab.net"
CERTIFICATE_URL = "https://certauth.swano-lab.net"

TRUST_APP_URL = "https://trust.swano-lab.net"

INTERNAL_APP = "http://localhost:8800"

ALLOWED_HOSTS = ["sso.swano-lab.net", "trust.swano-lab.net", "localhost", "certauth.swano-lab.net"]

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("DATABASE_NAME"),
        "USER": os.environ.get("DATABASE_USER"),
        "PASSWORD": os.environ.get("DATABASE_PASS"),
        "HOST": os.environ.get("DATABASE_HOST") or "127.0.0.1",
        "PORT": os.environ.get("DATABASE_PORT") or "5432",
    }
}

SESSION_COOKIE_SECURE = True

STATIC_ROOT = "/static"
