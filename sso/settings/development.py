from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "c6oaacw39z%*ebev_s^+9!d=ih)5e5t%c09ho04e)t0h7cp&lq"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

CERTIFICATE_URL = BASE_URL = "http://localhost:8000"

TRUST_APP_URL = "http://localhost:8000"

INTERNAL_APP = "http://localhost:8000"


ALLOWED_HOSTS = ["*"]

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}


STATIC_ROOT = "/static"
