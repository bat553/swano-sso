"""sso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#  swano-sso
#  urls.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25
from django.conf import settings
from django.conf.urls.static import static
from django.shortcuts import redirect
from django.urls import path, include

from app_sso.admin import admin_site


def redirect_login(request):
    return redirect("login_view")


urlpatterns = [
    path("trust_client/", include("trust_client.urls")),
    path("admin/login/", redirect_login),
    path("admin/logout/", redirect_login),
    path("admin/", admin_site.urls, name="admin"),
    path("", include("app_sso.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
