# Serveur d'authentification Swano Inc.
[![Python : 3.9.2](https://img.shields.io/badge/Python-3.9.2-00000.svg)](https://gitlab.com/bat553/swano-sso/-/commits/master)
[![Django](https://img.shields.io/badge/Django-DjangoRestFramework-a30000.svg)](https://gitlab.com/bat553/swano-sso/-/commits/master)

[![pipeline status](https://gitlab.com/bat553/swano-sso/badges/master/pipeline.svg)](https://gitlab.com/bat553/swano-sso/-/commits/master)
[![coverage report](https://gitlab.com/bat553/swano-sso/badges/master/coverage.svg)](https://gitlab.com/bat553/swano-sso/-/commits/master)

---

##### Installation des dépendances
````shell
python -m venv venv
source venv/bin/activate
pip instal -r requirements/dev.txt
````
##### Lancement du projet
````shell
export DJANGO_SETTINGS_MODULE=sso.settings.development
python manage.py migrate
python manage.py runserver
````
