from authlib.integrations.django_oauth2 import (
    AuthorizationServer,
    RevocationEndpoint,
    ResourceProtector,
    BearerTokenValidator,
)
from authlib.oauth2.rfc6749 import grants, InvalidRequestError, InvalidClientError, InvalidGrantError
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from app_sso.models import OAuth2Token, OAuth2Client, ReturnUrl, AuthorizationCode
from app_sso.utils import generic_error

server = AuthorizationServer(OAuth2Client, OAuth2Token)


class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic", "client_secret_post"]

    def save_authorization_code(self, code, request):
        client = request.client
        auth_code = AuthorizationCode(
            code=code,
            _client=client,
            redirect_uri=request.redirect_uri,
            response_type=str(request.response_type).lower(),
            scope=str(request.scope).lower(),
            user=request.user,
        )
        auth_code.save()
        return auth_code

    def query_authorization_code(self, code, client):
        try:
            item = AuthorizationCode.objects.get(code=code, _client=client)
        except AuthorizationCode.DoesNotExist:
            return None

        if not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        authorization_code.delete()

    def authenticate_user(self, authorization_code):
        return authorization_code.user


class RefreshTokenGrant(grants.RefreshTokenGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic", "client_secret_post"]

    def authenticate_refresh_token(self, refresh_token):
        try:
            item = OAuth2Token.objects.get(refresh_token=refresh_token)
            if item.is_refresh_token_active():
                return item
        except OAuth2Token.DoesNotExist:
            return None

    def authenticate_user(self, credential):
        return credential.user

    def revoke_old_credential(self, credential):
        credential.revoked = True
        credential.save()


server.register_grant(AuthorizationCodeGrant)
server.register_grant(RefreshTokenGrant)
server.register_endpoint(RevocationEndpoint)


def authorize(request):
    if request.method == "GET":
        if request.user.is_anonymous:
            return_url = ReturnUrl(url=request.get_raw_uri())
            return_url.save()
            request.session["return_url"] = return_url.pk
            messages.info(request, "Merci de vous connecter pour continuer")
            return redirect("index")
        try:
            grant = server.validate_consent_request(request, end_user=request.user)
        except (InvalidRequestError, InvalidClientError, InvalidGrantError) as e:
            return generic_error(e.description, e.error, status=400)
        hide = False
        if request.GET.get("hide_grant"):
            # Afficher le grant uniquement si l'utilisateur n'a jamais utilisé cette application
            tokens = OAuth2Token.objects.filter(user=request.user, _client__client_id=request.GET.get("client_id", None))
            # Existe hide == OK
            hide = True if len(tokens) else False

        if not hide:
            context = dict(
                grant=grant, utilisateur=request.user, title=f"Authorization {grant.client.client_name} | Swano SSO"
            )
            return render(request, "oauth2/authorize.html", context)

    if not request.user.is_anonymous and request.user.is_active:
        # granted by resource owner
        try:
            return server.create_authorization_response(request, grant_user=request.user)
        except InvalidRequestError as e:
            return generic_error(e.description, e.error, status=400)

    # denied by resource owner
    return server.create_authorization_response(request, grant_user=None)


# use ``server.create_token_response`` to handle token endpoint


@require_http_methods(["POST"])  # we only allow POST for token endpoint
@csrf_exempt
def issue_token(request):
    try:
        return server.create_token_response(request=request)
    except InvalidRequestError as e:
        return generic_error(e.description, e.error, status=400)


@require_http_methods(["POST", "GET"])
@csrf_exempt
def revoke_token(request):
    try:
        return server.create_endpoint_response(RevocationEndpoint.ENDPOINT_NAME, request)
    except InvalidRequestError as e:
        return generic_error(e.description, e.error, status=400)


require_oauth = ResourceProtector()
require_oauth.register_token_validator(BearerTokenValidator(OAuth2Token))
