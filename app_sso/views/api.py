from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .oauth2_provider import require_oauth


@require_http_methods(["GET"])
@require_oauth("")
def user_profile(request):
    user = request.oauth_token.user
    return JsonResponse(dict(sub=user.pk, email=user.email, nom=user.nom, prenom=user.prenom))
