import pyotp
from django import forms
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods

from app_sso.models import RecoveryToken, OTPToken


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(required=True)
    new_password = forms.CharField(required=True)
    confirm_password = forms.CharField(required=True)

    def clean(self):
        form_data = self.cleaned_data
        if form_data["new_password"] != form_data["confirm_password"]:
            self._errors["new_password"] = ["Les mots de passes ne correspondent pas."]  # Will raise a error message
            del form_data["new_password"]
        return form_data


@require_http_methods(["GET", "POST"])
def change_password_view(request):
    if not request.user.is_authenticated:
        messages.error(request, "Merci de vous connecter", extra_tags="danger")
        return redirect("login_view")

    title = "Changement de mot de passe"
    if request.method == "POST":
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            if not request.user.check_password(form.cleaned_data["current_password"]):
                messages.error(request, "Mot de passe courant invalide")
            else:
                hashed_password = make_password(form.cleaned_data["new_password"])
                request.user.password = hashed_password
                request.user.save()
                messages.success(request, "Mot de passe changé avec succès!")
                logout(request)
                return redirect("login_view")
        else:
            messages.error(request, form.errors, extra_tags="danger")

    form = ChangePasswordForm()

    return render(
        request,
        "reset_password.html",
        {"title": title, "errors": None, "form": form},
    )


class EnrollTOTPForm(forms.Form):
    secret = forms.CharField(required=True)
    code = forms.IntegerField(required=True)


@require_http_methods(["GET", "POST"])
def otp_enroll_view(request):
    if not request.user.is_authenticated:
        messages.error(request, "Merci de vous connecter")
        return redirect("login_view")
    title = "Activer la double authentification"

    if request.method == "POST":
        form = EnrollTOTPForm(request.POST)
        if form.is_valid():
            totp = pyotp.TOTP(form.cleaned_data["secret"])
            if totp.verify(form.cleaned_data["code"]):
                try:
                    OTPToken.objects.filter(utilisateur=request.user).delete()
                    RecoveryToken.objects.filter(utilisateur=request.user).delete()
                except OTPToken.DoesNotExist or RecoveryToken.DoesNotExist:
                    pass

                OTPToken.objects.create(utilisateur=request.user, token=form.cleaned_data["secret"])
                recovery_codes = []
                for _ in range(10):
                    recovery_code = RecoveryToken.objects.create(utilisateur=request.user)
                    recovery_codes.append(recovery_code.token)

                messages.success(request, "TOTP activé")
                return render(request, "TwoFA/otp/enroll_otp.html", {"title": title, "recovery_codes": recovery_codes})
            else:
                messages.error(request, "Code invalide", extra_tags="danger")
        else:
            messages.error(request, form.errors, extra_tags="danger")

    form = EnrollTOTPForm()

    return render(
        request,
        "TwoFA/otp/enroll_otp.html",
        {"title": title, "form": form, "secret": pyotp.random_base32(), "errors": None},
    )


class DisableOTPForm(forms.Form):
    password = forms.CharField(required=True)


@require_http_methods(["GET", "POST"])
def otp_disable_view(request):
    if not request.user.is_authenticated:
        messages.error(request, "Merci de vous connecter")
        return redirect("login_view")
    title = "Désactiver la double authentification"

    if request.method == "POST":
        form = DisableOTPForm(request.POST)
        if form.is_valid():
            if request.user.check_password(form.cleaned_data["password"]):
                try:
                    OTPToken.objects.filter(utilisateur=request.user).delete()
                    RecoveryToken.objects.filter(utilisateur=request.user).delete()
                except OTPToken.DoesNotExist or RecoveryToken.DoesNotExist:
                    pass

                messages.success(request, "TOTP désactivé")
                return redirect("index")
            else:
                messages.error(request, "Mot de passe incorrect", extra_tags="danger")
        else:
            messages.error(request, form.errors, extra_tags="danger")

    form = DisableOTPForm()

    return render(request, "TwoFA/otp/otp_disable.html", {"title": title, "form": form})
