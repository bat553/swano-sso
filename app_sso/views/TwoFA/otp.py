import pyotp
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.http import require_http_methods

from app_sso.models import Utilisateur
from app_sso.models.otp import OTPToken, RecoveryToken
from app_sso.utils import redirect_to, clean_2fa_session


@require_http_methods(["GET", "POST"])
def otp_view(request):
    if not request.session or not request.session.get("wait_for_2FA") or not request.session.get("2FA_user_id"):
        messages.error(request, "Merci de vous connecter", extra_tags="warning")
        return redirect(reverse("login_view"))

    if request.method == "POST":
        otp_code = request.POST.get("otp")
        try:
            utilisateur: Utilisateur = Utilisateur.objects.get(pk=request.session.get("2FA_user_id"))
        except Utilisateur.DoesNotExist:
            messages.error(request, "Merci de vous connecter (utilisateur introuvable)", extra_tags="warning")
            return redirect(reverse("login_view"))
        otp: OTPToken = utilisateur.get_otp()
        if not otp:
            messages.error(request, "Merci de vous connecter (pas de token otp)", extra_tags="warning")
            return redirect(reverse("login_view"))

        secret = otp.token
        totp = pyotp.TOTP(secret)
        if totp.verify(otp_code):
            login(request, utilisateur)
            clean_2fa_session(request)
            return redirect_to(request)
        messages.error(request, "Code OTP invalide", extra_tags="warning")

    return render(request, "TwoFA/otp/otp.html", {"title": "OTP", "errors": None})


@require_http_methods(["GET", "POST"])
def otp_recovery_view(request):
    if not request.session or not request.session.get("wait_for_2FA") or not request.session.get("2FA_user_id"):
        messages.error(request, "Merci de vous connecter", extra_tags="danger")
        return redirect(reverse("login_view"))

    if request.method == "POST":
        try:
            utilisateur: Utilisateur = Utilisateur.objects.get(pk=request.session.get("2FA_user_id"))
            recovery_code = RecoveryToken.objects.get(utilisateur=utilisateur, token=request.POST.get("recovery_code"))
            if recovery_code:
                recovery_code.delete()
                login(request, utilisateur)
                clean_2fa_session(request)
                return redirect_to(request)
        except Utilisateur.DoesNotExist:
            messages.error(request, "Merci de vous connecter (utilisateur introuvable)", extra_tags="danger")
            return redirect(reverse("login_view"))
        except RecoveryToken.DoesNotExist:
            messages.error(request, "Code introuvable, merci de réessayer", extra_tags="danger")
            return render(request, "TwoFA/otp/otp_recovery.html", {"title": "OTP Recovery", "errors": None})
        messages.error(request, "Code OTP invalide", extra_tags="danger")

    return render(request, "TwoFA/otp/otp_recovery.html", {"title": "OTP Recovery", "errors": None})
