from django.contrib import messages
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from app_sso.models import Utilisateur


@require_http_methods(["GET"])
def select_2fa(request):
    if not request.session or not request.session.get("wait_for_2FA") or not request.session.get("2FA_user_id"):
        messages.error(request, "Merci de vous connecter", extra_tags="danger")
        return redirect("login_view")

    try:
        utilisateur = Utilisateur.objects.get(pk=request.session.get("2FA_user_id"))
    except Utilisateur.DoesNotExist:
        messages.error(request, "Merci de vous connecter (utilisateur introuvable)", extra_tags="warning")
        return redirect("login_view")

    list_2fa = utilisateur.list_2FA()

    if len(list_2fa) > 1:
        return render(request, "TwoFA/select.html", {"title": "2FA sélection", "methods": list_2fa})
    else:
        return redirect(list_2fa[0]["view"])
