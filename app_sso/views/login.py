from captcha.fields import ReCaptchaField
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django import forms
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.http import require_http_methods

from app_sso.models import Utilisateur, OTPToken
from app_sso.utils import redirect_to, clean_2fa_session


class LoginForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True)
    captcha = ReCaptchaField()


@require_http_methods(["GET", "POST"])
def login_view(request):
    if request.user.is_authenticated:
        messages.success(request, "Vous êtes connecté")
        return redirect("index")
    title = "Connexion | Swano SSO"

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid() or settings.DEBUG:
            email = request.POST.get("email")
            password = request.POST.get("password")
            if email and password:
                user = authenticate(request, email=email, password=password)
                if user is not None:
                    utilisateur = Utilisateur.objects.get(email=email)
                    # Vérification sur 2FA activée
                    if len(utilisateur.list_2FA()):
                        request.session["wait_for_2FA"] = True
                        request.session["2FA_user_id"] = utilisateur.pk
                        return redirect("select_2FA")
                    login(request, user)
                    messages.success(request, "Vous êtes connecté!")

                    return redirect_to(request)

        messages.error(request, form.errors)
        return redirect("login_view")

    clean_2fa_session(request)
    form = LoginForm()

    return render(
        request,
        "login.html",
        {
            "title": title,
            "errors": None,
            "form": form,
            "utilisateur": request.user if request.user.is_authenticated else None,
        },
    )


@require_http_methods(["GET"])
def index(request):
    clean_2fa_session(request)
    otp = None
    if request.user.is_authenticated:
        try:
            otp = OTPToken.objects.get(utilisateur=request.user)
        except OTPToken.DoesNotExist:
            pass

    return render(
        request,
        "index.html",
        {
            "title": "SSO | Swano Inc.",
            "otp": otp,
            "utilisateur": request.user if request.user.is_authenticated else None,
            "certificate_url": settings.CERTIFICATE_URL,
        },
    )


@require_http_methods(["POST"])
def logout_view(request):
    logout(request)
    clean_2fa_session(request)

    messages.success(request, "Vous êtes déconnecté!")
    return redirect("index")
