import uuid

from django.conf import settings
from django.contrib.auth import login
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods

from app_sso.models import Utilisateur
from app_sso.utils import redirect_to


@require_http_methods(["GET"])
def check_certificate(request):
    """
    Cet endpoint doit être protégé par un certificat client
    X-SSL-SN : ssl_client_serial
    :param request:
    :return:
    """
    if ("X-SSL-SN" not in request.headers or not request.headers.get("X-SSL-SN")) and not request.GET.get("code"):
        return JsonResponse(data=dict(error="Merci de spécifier un certificat"), status=401)

    if not request.GET.get("code"):
        try:
            utilisateur = Utilisateur.objects.get(certificate_sn=request.headers.get("X-SSL-SN"))
            utilisateur.certificate_code = uuid.uuid4().hex
            utilisateur.save(update_fields=["certificate_code"])
            return redirect(f"{settings.BASE_URL}/certificate?code=" + utilisateur.certificate_code)
        except Utilisateur.DoesNotExist:
            return JsonResponse(data=dict(error="Certificat introuvable"), status=401)

    else:
        try:
            utilisateur = Utilisateur.objects.get(certificate_code=request.GET.get("code"))
            utilisateur.certificate_code = None
            utilisateur.save(update_fields=["certificate_code"])
            login(request, utilisateur)
            return redirect_to(request)
        except Utilisateur.DoesNotExist:
            return JsonResponse(data=dict(error="Certificat introuvable"), status=401)
