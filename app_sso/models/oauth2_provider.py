import time
import uuid

from authlib.oauth2.rfc6749 import ClientMixin, TokenMixin, AuthorizationCodeMixin, InvalidRequestError
from django.db.models import Model, URLField, ForeignKey, CharField, TextField, CASCADE, BooleanField, IntegerField, \
    DateTimeField


def generate_secret():
    return uuid.uuid4().hex


class OAuth2Client(Model, ClientMixin):
    SCOPE = (("profile", "Voir le profile d'un utilisateur"), ("admin", "Administer le compte (pas implémenté)"))

    user = ForeignKey("app_sso.Utilisateur", on_delete=CASCADE)
    client_id = CharField(max_length=48, unique=True, db_index=True, default=generate_secret)
    client_secret = CharField(max_length=48, null=False, default=generate_secret)
    client_name = CharField(max_length=120)
    redirect_uris = TextField(default="", blank=False, help_text="Séparé par des espaces")
    scope = CharField(max_length=120, choices=SCOPE)
    enabled = BooleanField(default=True, help_text="Application active", verbose_name="Activée")
    web_site = URLField(help_text="Url de l'application (décoration)")
    company = CharField(max_length=240, help_text="Nom de l'entreprise (décoratif)")
    # you can add more fields according to your own need
    # check https://tools.ietf.org/html/rfc7591#section-2

    class Meta:
        verbose_name = "Client OAuth2"
        verbose_name_plural = "Clients OAuth2"

    ALLOWED_METHODS = (
        "client_secret_basic",
        "client_secret_post",
    )
    ALLOWED_RESPONSE_TYPE = (
        "token",
        "code",
    )
    ALLOWED_GRANT_TYPE = ("authorization_code",)
    ALLOWED_SCOPES = (
        "admin",
        "profile",
    )

    def __str__(self):
        return self.client_name

    def get_client_id(self):
        return self.client_id

    def get_default_redirect_uri(self):
        return self.redirect_uris.split()[0]

    def get_allowed_scope(self, scope):
        if scope in self.ALLOWED_SCOPES:
            return scope
        raise InvalidRequestError("Scope invalide")

    def check_redirect_uri(self, redirect_uri):
        return redirect_uri in set(self.redirect_uris.split())

    def has_client_secret(self):
        return bool(self.client_secret)

    def check_client_secret(self, client_secret):
        return self.client_secret == client_secret

    def check_allowed_scope(self, scope):
        return scope in self.ALLOWED_SCOPES

    def check_endpoint_auth_method(self, method, endpoint):
        return method in self.ALLOWED_METHODS

    def check_token_endpoint_auth_method(self, method):
        return method in self.ALLOWED_METHODS

    def check_response_type(self, response_type):
        return response_type in self.ALLOWED_RESPONSE_TYPE

    def check_grant_type(self, grant_type):
        return grant_type in self.ALLOWED_GRANT_TYPE


class OAuth2Token(Model, TokenMixin):
    user = ForeignKey("app_sso.Utilisateur", on_delete=CASCADE)
    _client = ForeignKey(OAuth2Client, db_index=True, on_delete=CASCADE, db_column="client")
    token_type = CharField(max_length=40)
    access_token = CharField(max_length=255, unique=True, null=False)
    refresh_token = CharField(max_length=255, db_index=True)
    scope = CharField(max_length=50)
    revoked = BooleanField(default=False)
    issued_at = IntegerField(null=False, default=time.time)
    timestamp = DateTimeField(null=False, auto_now_add=True)
    expires_in = IntegerField(null=False, default=3600)

    def __str__(self):
        return "%s - %s - %s" % (self._client, self.user, self.timestamp)

    @property
    def client_id(self):
        return self._client.client_id

    @client_id.setter
    def client_id(self, client_id):
        try:
            self._client = OAuth2Client.objects.get(client_id=client_id)
        except OAuth2Client.DoesNotExist as e:
            raise e

    def get_client_id(self):
        return self._client.client_id

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_expires_at(self):
        return self.issued_at + self.expires_in

    def clean(self):
        if self.scope != self._client.scope:
            raise InvalidRequestError("Scope non autorisé")

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = "OAuth2 Token"
        verbose_name_plural = "OAuth2 Tokens"


class AuthorizationCode(Model, AuthorizationCodeMixin):
    user = ForeignKey("app_sso.Utilisateur", on_delete=CASCADE)
    _client = ForeignKey(OAuth2Client, db_index=True, on_delete=CASCADE, db_column="client")
    code = CharField(max_length=120, unique=True, null=False)
    redirect_uri = CharField(max_length=250)
    response_type = CharField(max_length=50)
    scope = CharField(max_length=50)
    auth_time = IntegerField(null=False, default=time.time)

    @property
    def client_id(self):
        return self._client.client_id

    @client_id.setter
    def client_id(self, client_id):
        try:
            self._client = OAuth2Client.objects.get(client_id=client_id)
        except OAuth2Client.DoesNotExist as e:
            raise e

    def is_expired(self):
        return self.auth_time + 300 < time.time()

    def get_redirect_uri(self):
        return self.redirect_uri

    def get_scope(self):
        return self.scope

    def get_auth_time(self):
        return self.auth_time

    def clean(self):
        if self.redirect_uri not in set(self._client.redirect_uris.split()):
            raise InvalidRequestError("Redirect URI non référencé")
        if self.scope != self._client.scope:
            raise InvalidRequestError("Scope non autorisé")
        if self.response_type not in self._client.ALLOWED_RESPONSE_TYPE:
            raise InvalidRequestError("Response type non autorisé")
        if self._client.enabled is False:
            raise InvalidRequestError("Client désactivé")
        if self._client not in self.user.allowed_apps.all():
            raise InvalidRequestError("Vous n'avez pas accès à cette application")

    def save(self, *args, **kwargs):
        self.clean()
        return super(AuthorizationCode, self).save(*args, **kwargs)
