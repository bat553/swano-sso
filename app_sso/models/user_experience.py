from django.db.models import Model, TextField, DateTimeField


class ReturnUrl(Model):
    url = TextField(blank=False)
    timestamp = DateTimeField(auto_now_add=True, editable=False)
