import uuid

import pyotp
from django.db import models
from django.db.models import Model


class OTPToken(Model):
    utilisateur = models.OneToOneField("app_sso.Utilisateur", on_delete=models.CASCADE)
    token = models.CharField(max_length=120, default=pyotp.random_base32, editable=False, unique=True)
    issued_at = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.utilisateur.email


class RecoveryToken(Model):
    utilisateur = models.ForeignKey("app_sso.Utilisateur", on_delete=models.CASCADE, editable=False)
    token = models.CharField(max_length=120, default=uuid.uuid4, editable=False, unique=True)
    issued_at = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.utilisateur.email
