import uuid

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from app_sso.models.otp import OTPToken


class UserManager(BaseUserManager):
    def create_user(self, email, nom, prenom, password=None):
        if not email:
            raise ValidationError("Manque votre email")
        user = Utilisateur(email=email, nom=nom, prenom=prenom)
        user.set_password(password)

        user.save(using=self._db)

        return user

    def create_superuser(self, email, nom, prenom, password):
        user = self.create_user(email, nom, prenom, password)

        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)

        return user


class Utilisateur(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_("email address"), unique=True)
    nom = models.CharField(max_length=254)
    prenom = models.CharField(max_length=254)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    allowed_apps = models.ManyToManyField("app_sso.OAuth2Client", verbose_name="Application autorisées", blank=True)
    certificate_sn = models.CharField(
        max_length=255, null=True, blank=True, unique=True, help_text="Numéro de série du certificat"
    )
    certificate_code = models.CharField(null=True, editable=True, max_length=64)
    password = models.CharField(_('password'), max_length=128, blank=True, default=uuid.uuid4)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["nom", "prenom"]

    def __str__(self):
        return self.email

    def get_otp(self):
        try:
            return OTPToken.objects.get(utilisateur=self)
        except OTPToken.DoesNotExist:
            return None

    def list_2FA(self) -> list:
        """
        A plusieurs méthodes de double authentification
        :return:
        """
        twofa_methods = {
            "OTP": {"view": "otp_view", "name": "TOTP"},
            "webauthn": {"view": "webauthn_view", "name": "WebAuthn"},
        }

        twofa_methods["OTP"]["view"] = reverse("otp_view")
        twofa_methods["webauthn"]["view"] = reverse("webauthn_view")

        list_2fa = []
        if self.get_otp():
            list_2fa.append(twofa_methods.get("OTP"))
        if False: # A dev
            list_2fa.append(twofa_methods.get("webauthn"))

        return list_2fa

    class Meta:
        verbose_name = "Utilisateur"

class LoginRecord(models.Model):
    RECORD_TYPES = (
        (1, "Login",),
        (2, "Logout",),
        (3, "Failed",),
    )

    timestamp = models.DateTimeField(auto_now=True, editable=False)
    user = models.ForeignKey(Utilisateur, on_delete=models.CASCADE, null=True)
    data = models.TextField(max_length=255, null=True)
    record_type = models.IntegerField(choices=RECORD_TYPES)

    def __str__(self):
        return "%s" % (self.user.email if self.user else self.data[:15])
