#  swano-sso
#  admin.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25

#  swano-sso
#  admin.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25

from gettext import ngettext
from hmac import compare_digest

from django.contrib import admin, messages

# Register your models here.
from django.contrib.admin.models import LogEntry
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import Group

from app_sso.models import OAuth2Client, Utilisateur, OAuth2Token, LoginRecord
from app_sso.models.otp import OTPToken


class CustomAdminSite(admin.AdminSite):
    site_title = "Swano SSO"
    site_header = "Administration du SSO"


admin_site = CustomAdminSite(name="customadmin")

admin_site.register(Group)


@admin.register(LogEntry, site=admin_site)
class LogEntryAdmin(admin.ModelAdmin):
    list_filter = (
        "action_time",
        "user",
        "content_type",
        "action_flag"
    )

    list_display = (
        "user",
        "content_type",
        "action_flag"
    )

    readonly_fields = (
        "object_repr",
        "object_id",
        "change_message",
        "action_time",
        "user",
        "content_type",
        "action_flag"
    )
    date_hierarchy = 'action_time'


@admin.register(LoginRecord, site=admin_site)
class LoginRecordAdmin(admin.ModelAdmin):
    list_filter = (
        "user",
        "record_type"
    )
    list_display = (
        "timestamp",
        "user",
        "record_type"
    )
    readonly_fields = (
        "timestamp",
        "user",
        "record_type",
        "data"
    )
    date_hierarchy = 'timestamp'


@admin.register(OAuth2Token, site=admin_site)
class OAuth2TokenAdmin(admin.ModelAdmin):
    list_display = (
        "_client",
        "user",
        "revoked",
        "timestamp"
    )
    list_filter = (
        "_client",
        "user",
        "revoked",
    )
    readonly_fields = (
        "_client",
        "user",
        "timestamp"
    )
    fields = (
        "_client",
        "user",
        "revoked",
        "timestamp"
    )
    date_hierarchy = 'timestamp'


@admin.register(OAuth2Client, site=admin_site)
class OAuth2ClientAdmin(admin.ModelAdmin):
    list_display = (
        "client_name",
        "user",
        "enabled",
    )
    readonly_fields = (
        "client_id",
        "client_secret",
    )
    fields = (
        "client_name",
        "client_id",
        "client_secret",
        "redirect_uris",
        "scope",
        "user",
        "web_site",
        "enabled",
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields["user"].initial = request.user
        return form


@admin.register(Utilisateur, site=admin_site)
class UtilisateurAdmin(admin.ModelAdmin):
    readonly_fields = ("last_login",)
    list_display = (
        "email",
        "prenom",
        "nom",
    )
    fieldsets = (
        (
            "Utilisateur",
            {
                "fields": (
                    (
                        "nom",
                        "prenom",
                    ),
                    "email",
                )
            },
        ),
        (
            "oAuth",
            {"fields": ("allowed_apps",)},
        ),
        (
            "Administration",
            {
                "fields": (
                    "certificate_sn",
                    "password",
                    "is_staff",
                    "is_active",
                    "is_superuser",
                    "last_login",
                    "groups",
                ),
                "classes": ("collapse",),
            },
        ),
    )
    actions = ["oauth2_logout"]

    def oauth2_logout(self, request, queryset):
        i = 0
        for obj in queryset:
            tokens = OAuth2Token.objects.filter(user=obj, revoked=False)
            i += len(tokens)
            try:
                tokens.update(revoked=True)
            except Exception as e:
                self.message_user(request, "Erreur de traitement", messages.ERROR)

        self.message_user(request, ngettext("%d token révoqué", "%d tokens révoqués", i) % i, messages.SUCCESS)

    def save_model(self, request, obj, form, change):
        try:
            user_database = Utilisateur.objects.get(pk=obj.pk)
            if request.user and (request.user.pk == obj.pk or request.user.is_superuser):
                if not (
                        check_password(form.data["password"], user_database.password)
                        or compare_digest(user_database.password, form.data["password"])
                ):
                    obj.password = make_password(obj.password)
                else:
                    obj.password = user_database.password
            else:
                obj = user_database
        except Utilisateur.DoesNotExist:
            pass
        super().save_model(request, obj, form, change)


@admin.register(OTPToken, site=admin_site)
class OTPTokenAdmin(admin.ModelAdmin):
    fields = (
        "utilisateur",
        "token",
        "issued_at",
    )
    readonly_fields = (
        "token",
        "issued_at",
    )
