#  swano-sso
#  apps.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25
from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class AppSsoConfig(AppConfig):
    name = "app_sso"
    verbose_name = "Swano Inc. SSO"


class CustomAdmin(AdminConfig):
    default_site = "app_sso.admin.CustomAdminSite"
