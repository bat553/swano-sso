import uuid

from django.test import TestCase, Client

from app_sso.models import OAuth2Client, OAuth2Token
from app_sso.models.oauth2_provider import generate_secret, AuthorizationCode
from app_sso.models.otp import OTPToken, RecoveryToken
from app_sso.models.utilisateurs import Utilisateur


class TestCommon(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = Utilisateur.objects.create_superuser(
            "admin@swano-lab.net", "ADMINISTRATEUR", "Swano", password="123456789"
        )
        cls.user = Utilisateur.objects.create_superuser(
            "user@swano-lab.net", "UTILISATEUR", "Swano", password="123456789"
        )

        cls.oauth2_client_1 = OAuth2Client.objects.create(
            user=cls.admin, client_name="Client1", redirect_uris="http://google.com http://google.fr", scope="profile"
        )
        cls.admin.allowed_apps.add(cls.oauth2_client_1)

        cls.client_admin = Client()
        cls.client_admin.login(email=cls.admin, password="123456789")

        cls.client_anon = Client()

        cls.oauth2_token_1 = OAuth2Token.objects.create(
            user=cls.admin,
            _client=cls.oauth2_client_1,
            token_type="Bearer",
            access_token=generate_secret(),
            scope="profile",
            expires_in=3600,
        )

        cls.oauth2_authorization_code_1 = AuthorizationCode.objects.create(
            user=cls.admin,
            _client=cls.oauth2_client_1,
            code=uuid.uuid4().hex,
            redirect_uri=cls.oauth2_client_1.redirect_uris.split(" ")[0],
            response_type="code",
            scope="profile",
        )

        cls.otp_admin = OTPToken.objects.create(utilisateur=cls.admin)
        cls.otp_recovery_admin = RecoveryToken.objects.create(utilisateur=cls.admin)
