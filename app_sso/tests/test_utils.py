from app_sso.tests.test_common import TestCommon
from app_sso.utils import generic_error, permission_error


class TestUtils(TestCommon):
    def test_generic_error(self):
        status = 509
        response = generic_error("test", "test_error", status)
        self.assertEqual(response.status_code, status)
        self.assertEqual(response.content, b'{"error": "test_error", "error_description": "test"}')

    def test_permission_error(self):
        response = permission_error()
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.content, b'{"error": "permission_denied", "error_description": "Permission refus\\u00e9e"}'
        )
