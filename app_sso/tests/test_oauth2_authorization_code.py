import time

from authlib.oauth2.rfc6749 import InvalidRequestError

from app_sso.models import AuthorizationCode, OAuth2Client
from app_sso.tests.test_common import TestCommon


class TestAuthorizationCode(TestCommon):
    def test_get_client_id(self):
        self.assertEqual(self.oauth2_authorization_code_1.client_id, self.oauth2_client_1.client_id)

    def test_set_client_id_succeed(self):
        self.oauth2_authorization_code_1.client_id = self.oauth2_client_1.client_id

    def test_set_client_id_failed(self):
        def set_client():
            self.oauth2_authorization_code_1.client_id = "random"

        self.assertRaises(OAuth2Client.DoesNotExist, set_client)

    def test_is_not_expired(self):
        self.assertFalse(self.oauth2_authorization_code_1.is_expired())

    def test_is_expired(self):
        authorization_code = AuthorizationCode(auth_time=(time.time() - 301))

        self.assertTrue(authorization_code.is_expired())

    def test_redirect_uri(self):
        self.assertEqual(
            self.oauth2_authorization_code_1.get_redirect_uri(), self.oauth2_client_1.redirect_uris.split(" ")[0]
        )

    def test_get_scope(self):
        self.assertEqual(self.oauth2_authorization_code_1.get_scope(), "profile")

    def test_get_auth_time(self):
        self.assertAlmostEqual(self.oauth2_authorization_code_1.get_auth_time(), time.time(), places=1)

    def test_check_parameters_redirect_uri(self):
        authorization_code = AuthorizationCode(
            _client=self.oauth2_client_1, redirect_uri="not referenced", scope="profile", response_type="code"
        )
        self.assertRaises(InvalidRequestError, authorization_code.clean)

    def test_check_parameters_scope(self):
        authorization_code = AuthorizationCode(
            _client=self.oauth2_client_1, redirect_uri="http://google.com", scope="not referenced", response_type="code"
        )
        self.assertRaises(InvalidRequestError, authorization_code.clean)

    def test_check_parameters_response_type(self):
        authorization_code = AuthorizationCode(
            _client=self.oauth2_client_1,
            redirect_uri="http://google.com",
            scope="profile",
            response_type="not referenced",
        )
        self.assertRaises(InvalidRequestError, authorization_code.clean)

    def test_check_parameters_client_disabled(self):
        client = OAuth2Client(enabled=False, redirect_uris="http://google.com", scope="profile")
        authorization_code = AuthorizationCode(
            _client=client, redirect_uri="http://google.com", scope="profile", response_type="code"
        )
        self.assertRaises(InvalidRequestError, authorization_code.clean)

    def test_clean_succeed(self):
        self.oauth2_authorization_code_1.clean()

    def test_save_succeed(self):
        self.oauth2_authorization_code_1.save()
