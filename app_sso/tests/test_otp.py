from app_sso.tests.test_common import TestCommon


class TestOTPToken(TestCommon):
    def test_otp_str(self):
        self.assertEqual(str(self.otp_admin), self.admin.email)

    def test_otp_recovery_str(self):
        self.assertEqual(str(self.otp_recovery_admin), self.admin.email)
