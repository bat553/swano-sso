from app_sso.admin import OAuth2ClientAdmin, CustomAdminSite, UtilisateurAdmin
from app_sso.models import OAuth2Client, Utilisateur, OAuth2Token
from app_sso.models.oauth2_provider import generate_secret
from app_sso.tests.test_common import TestCommon


class MockRequest:
    def __init__(self, user):
        self.user: Utilisateur = user

    class _messages(object):
        message = None

        @classmethod
        def show(cls):
            return cls.message

        @classmethod
        def add(cls, level, message, extra_tags=None):
            cls.message = f"{level}, {message}, {extra_tags}"


custom_admin = CustomAdminSite()


class TestOAuth2ClientAdmin(TestCommon):
    def test_get_form(self):
        model = OAuth2ClientAdmin(OAuth2Client, admin_site=custom_admin)
        request = MockRequest(self.admin)
        form = model.get_form(request)
        self.assertEqual(form.base_fields["user"].initial, self.admin)

    def test_oauth2_logout(self):
        utilisateur = Utilisateur.objects.create(email="test@test.com", nom="Test", prenom="Test")
        for _ in range(100):
            OAuth2Token.objects.create(
                user=utilisateur,
                token_type="Bearer",
                _client=self.oauth2_client_1,
                access_token=generate_secret(),
                scope="profile",
            )
        model = UtilisateurAdmin(Utilisateur, admin_site=custom_admin)
        queryset = Utilisateur.objects.filter(email=utilisateur.email)
        request = MockRequest(utilisateur)
        model.oauth2_logout(request, queryset)
        self.assertEqual(str(request._messages.show()), "25, 100 tokens révoqués, ")
