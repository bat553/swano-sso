from app_sso.tests.test_common import TestCommon


class TestDRFAPI(TestCommon):
    def test_user_profile_failed(self):
        response = self.client_anon.get("/api/me")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.reason_phrase, "Unauthorized")

    def test_user_profile_bad_auth_failed(self):
        headers = {"HTTP_AUTHORIZATION": f"Bearer {self.oauth2_token_1.access_token}544564564"}
        response = self.client_anon.get("/api/me", **headers)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.reason_phrase, "Unauthorized")

    def test_user_profile_succeed(self):
        headers = {"HTTP_AUTHORIZATION": f"Bearer {self.oauth2_token_1.access_token}"}
        response = self.client_admin.get("/api/me", **headers)
        self.assertEqual(response.status_code, 200)

    def test_statut(self):
        response = self.client_anon.get("/statut")
        self.assertEqual(response.status_code, 200)
