from authlib.oauth2.rfc6749 import InvalidRequestError

from app_sso.models.oauth2_provider import generate_secret
from app_sso.tests.test_common import TestCommon


class Oauth2ProviderModelTest(TestCommon):
    def test_generate_secret(self):
        """
        Vérification de l'entropie du la fonction
        :return:
        """
        ids: list = []
        # Generation d'uuid
        for _ in range(10000):
            ids.append(generate_secret())

        self.assertEqual(len(ids), len(set(ids)))

    def test_client_1_getters(self):
        self.assertEqual(self.oauth2_client_1.__str__(), "Client1")

        self.assertEqual(self.oauth2_client_1.get_client_id(), self.oauth2_client_1.client_id)

        self.assertEqual(self.oauth2_client_1.get_default_redirect_uri(), "http://google.com")

        self.assertTrue(self.oauth2_client_1.has_client_secret())

    def test_get_allowed_scope(self):
        def invalid_scope():
            self.oauth2_client_1.get_allowed_scope("bad")

        self.assertEqual(self.oauth2_client_1.get_allowed_scope("profile"), "profile")
        self.assertRaises(InvalidRequestError, invalid_scope)

    def test_check_client_secret(self):
        self.assertTrue(self.oauth2_client_1.check_client_secret(self.oauth2_client_1.client_secret))

    def test_check_redirect_uri(self):
        self.assertTrue(self.oauth2_client_1.check_redirect_uri("http://google.com"))
        self.assertFalse(self.oauth2_client_1.check_redirect_uri("http://bad.url"))

    def test_check_allowed_scope(self):
        self.assertTrue(self.oauth2_client_1.check_allowed_scope("profile"))
        self.assertFalse(self.oauth2_client_1.check_allowed_scope("bad_scope"))

    def test_check_endpoint_auth_method(self):
        self.assertTrue(self.oauth2_client_1.check_endpoint_auth_method("client_secret_post", None))
        self.assertFalse(self.oauth2_client_1.check_endpoint_auth_method("bad_method", None))

    def test_check_token_endpoint_auth_method(self):
        self.assertTrue(self.oauth2_client_1.check_token_endpoint_auth_method("client_secret_basic"))
        self.assertFalse(self.oauth2_client_1.check_token_endpoint_auth_method("bad_method"))

    def test_check_response_type(self):
        self.assertTrue(self.oauth2_client_1.check_response_type("code"))
        self.assertFalse(self.oauth2_client_1.check_response_type("bad_response_type"))

    def test_check_grant_type(self):
        self.assertTrue(self.oauth2_client_1.check_grant_type("authorization_code"))
        self.assertFalse(self.oauth2_client_1.check_grant_type("bad_grant_type"))
