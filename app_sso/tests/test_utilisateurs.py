from django.core.exceptions import ValidationError

from app_sso.models import Utilisateur
from app_sso.tests.test_common import TestCommon


class UtilisateursTest(TestCommon):
    def test_create_user_succeed(self):
        utilisateur = Utilisateur.objects.create_user(
            email="test@gmail.com", nom="NDF", prenom="PNOM", password="123456789"
        )
        utilisateur.save()

        self.assertNotEqual(utilisateur.password, "123456789")
        self.assertTrue(utilisateur.pk)
        self.assertFalse(utilisateur.is_staff)
        self.assertFalse(utilisateur.is_superuser)
        self.assertEqual(utilisateur.email, utilisateur.__str__())

    def test_create_user_failed(self):
        def create():
            Utilisateur.objects.create_user(email=None, nom="NDF", prenom="PNOM", password="123456789")

        self.assertRaises(ValidationError, create)

    def test_otp_admin(self):
        self.assertTrue(self.admin.get_otp())

    def test_otp_user(self):
        self.assertIsNone(self.user.get_otp())
