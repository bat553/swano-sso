from authlib.oauth2.rfc6749 import InvalidRequestError

from app_sso.models import OAuth2Client, OAuth2Token
from app_sso.tests.test_common import TestCommon


class Oauth2TokenModelTest(TestCommon):
    def test_client_id(self):
        def set_client_id_failed():
            self.oauth2_token_1.client_id = "bad_client_id"

        self.assertEqual(self.oauth2_token_1.client_id, self.oauth2_client_1.client_id)
        self.assertRaises(OAuth2Client.DoesNotExist, set_client_id_failed)
        self.oauth2_token_1.client_id = self.oauth2_client_1.client_id

    def test_getter(self):
        self.assertEqual(self.oauth2_token_1.get_client_id(), self.oauth2_client_1.client_id)
        self.assertEqual(self.oauth2_token_1.get_scope(), "profile")
        self.assertEqual(self.oauth2_token_1.get_expires_in(), 3600)
        self.assertEqual(
            self.oauth2_token_1.get_expires_at(), self.oauth2_token_1.issued_at + self.oauth2_token_1.expires_in
        )

    def test_check_parameters(self):
        token = OAuth2Token(
            user=self.admin,
            _client=self.oauth2_client_1,
            token_type="Bearer",
            access_token="ofeeffe",
            scope="bad_scope",
        )
        self.oauth2_token_1.clean()
        self.assertRaises(InvalidRequestError, token.save)
        self.assertRaises(InvalidRequestError, token.clean)
