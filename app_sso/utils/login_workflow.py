from django.contrib.auth import user_logged_in, user_logged_out, user_login_failed
from django.dispatch import receiver
from django.shortcuts import redirect

from app_sso.models import ReturnUrl, Utilisateur, LoginRecord


def redirect_to(request, default_url="index", use_reverse=True):
    """

    :param use_reverse: Utilise le reverse
    :param request:
    :param default_url: Url
    :return:
    """
    url = None
    if "return_url" in request.session:
        try:
            return_url = ReturnUrl.objects.get(pk=request.session.get("return_url"))
            request.session["return_url"] = None
            url = return_url.url
            return_url.delete()
        except ReturnUrl.DoesNotExist:
            pass

    return redirect(url or default_url)


def clean_2fa_session(request):
    if request.session:
        try:
            del request.session["wait_for_2FA"]
            del request.session["2FA_user_id"]
        except KeyError:
            pass


@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):
    LoginRecord.objects.create(
        user=user,
        record_type=1
    )

@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):
    LoginRecord.objects.create(
        user=user,
        record_type=2
    )


@receiver(user_login_failed)
def user_login_failed_callback(sender, request, **kwargs):
    if kwargs and kwargs.get("credentials"):
        credentials = kwargs.get("credentials")
        utilisateur = None
        try:
            utilisateur = Utilisateur.objects.get(email=credentials.get("email"))
            details = None
        except Utilisateur.DoesNotExist:
            if len(credentials.get("email")) > 255:
                details = credentials.get("email")[:250]
                details += "..."
            else:
                details = credentials.get("email")

        LoginRecord.objects.create(
            user=utilisateur,
            record_type=3,
            data=details
        )
