from django.http import JsonResponse


def generic_error(description, error, status) -> JsonResponse:
    return JsonResponse({"error": error, "error_description": description}, status=status)


def permission_error() -> JsonResponse:
    return generic_error("Permission refusée", "permission_denied", 403)
