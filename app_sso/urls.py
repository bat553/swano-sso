#  swano-sso
#  urls.py
#
#  Created by Baptiste PELLARIN 16/03/2021 23:25
from django.http import JsonResponse
from django.urls import path

from app_sso import views


def statut_view(request, format=None):
    return JsonResponse({"statut": "OK"}, status=200)

# Public website
urlpatterns = [
    path("login", views.login_view, name="login_view"),
    path("logout", views.logout_view, name="logout_view"),
    path("change_password", views.change_password_view, name="change_password_view"),
    path("statut", statut_view, name="statut_view"),
    path("", views.index, name="index"),
]

# 2FA endpoints
urlpatterns += [
    path("2FA/select", views.select_2fa, name="select_2FA"),
]

# WebAuthn Endpoints
urlpatterns += [path("webauthn", statut_view, name="webauthn_view")]

# Certificate Endpoint
urlpatterns += [path("certificate", views.check_certificate, name="certificate_view")]

# TOTP endpoints
urlpatterns += [
    path("otp", views.otp_view, name="otp_view"),
    path("otp/recovery", views.otp_recovery_view, name="otp_recovery_view"),
    path("otp/enroll", views.otp_enroll_view, name="otp_enroll_view"),
    path("otp/disable", views.otp_disable_view, name="otp_disable_view"),
]

# API
urlpatterns += [
    path("api/me", views.user_profile, name="api_user_profile"),
]

# oAuth2
urlpatterns += [
    path("oauth2/authorize", views.authorize, name="oauth2_authorize"),
    path("oauth2/access_token", views.issue_token, name="oauth2_access_token"),
    path("oauth2/revoke", views.revoke_token, name="oauth2_revoke_token"),
]
