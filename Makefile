black:
	black -l 120 --exclude=venv .

coverage:
	coverage run --source='.' manage.py test --settings=sso.settings.development
	coverage html
